package com.atlassian.translations.inproduct.actions;

import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.atlassian.translations.inproduct.components.TranslationManager;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * User: kalamon
 * Date: 17.07.12
 * Time: 10:59
 */
@WebSudoRequired
public class TranslationsManagementAction extends JiraWebActionSupport {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("i18n.admini18n");

    private final TranslationManager translationManager;
    private final WebResourceManager wrm;
    private final XsrfTokenGenerator xsrfTokenGenerator;

    public TranslationsManagementAction(
            TranslationManager translationManager, WebResourceManager wrm, XsrfTokenGenerator xsrfTokenGenerator) {
        this.translationManager = translationManager;
        this.wrm = wrm;
        this.xsrfTokenGenerator = xsrfTokenGenerator;
    }

    public boolean hasPermissions() {
        return hasPermission(Permissions.SYSTEM_ADMIN);

//        return hasGlobalPermission(GlobalPermissionKey.SYSTEM_ADMIN);
    }

    public Locale getCurrentLocale() {
        return getI18nHelper().getLocale();
    }

    public Map<String, String> getTranslations() {
        Locale currentLocale = getI18nHelper().getLocale();
        return translationManager.getAllTranslations(currentLocale.toString());
    }

    public String getProduct() {
        return "JIRA";
    }

    public String getName() {
        return "InProductTranslationsManagement.jspa";
    }

    @Override
    public String getText(String key) {
        return bundle.getString(key);
    }

    @Override
    protected String doExecute() throws Exception {
        wrm.requireResource("com.atlassian.translations.jira.inproduct:admin-css");
        boolean allowed = hasPermissions();
        if (allowed && request.getMethod().equals("POST")) {
            boolean ok = xsrfTokenGenerator.validateToken(request, request.getParameter("atl_token"));
            if (ok) {
                translationManager.deleteTranslation(getI18nHelper().getLocale().toString(), request.getParameter("keytodelete"));
//                accessManager.setGroups(request.getParameterValues("groups"));
            } else {
                return PERMISSION_VIOLATION_RESULT;
            }
        }
        return allowed ? SUCCESS : PERMISSION_VIOLATION_RESULT;
    }
}
