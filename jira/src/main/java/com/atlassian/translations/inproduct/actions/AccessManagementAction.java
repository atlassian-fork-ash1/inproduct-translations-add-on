package com.atlassian.translations.inproduct.actions;

import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.atlassian.translations.inproduct.components.JiraAccessManager;

import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * User: kalamon
 * Date: 17.07.12
 * Time: 10:59
 */
@WebSudoRequired
public class AccessManagementAction extends JiraWebActionSupport {

    private static final ResourceBundle bundle = ResourceBundle.getBundle("i18n.admini18n");

    private final JiraAccessManager accessManager;
    private final WebResourceManager wrm;
    private final XsrfTokenGenerator xsrfTokenGenerator;

    public AccessManagementAction(
            JiraAccessManager accessManager, WebResourceManager wrm, XsrfTokenGenerator xsrfTokenGenerator) {
        this.accessManager = accessManager;
        this.wrm = wrm;
        this.xsrfTokenGenerator = xsrfTokenGenerator;
    }

    public boolean hasPermissions() {
        return hasPermission(Permissions.SYSTEM_ADMIN);
//        return hasGlobalPermission(GlobalPermissionKey.SYSTEM_ADMIN);
    }

	/**
	 *
	 * @return list of groups available in JIRA (we skip faulty groups with new line characters in name)
	 */
    public List<JiraAccessManager.GroupSpec> getGroups() {
		List<JiraAccessManager.GroupSpec> groups = new CopyOnWriteArrayList<JiraAccessManager.GroupSpec>(accessManager.getGroups());

		for (JiraAccessManager.GroupSpec group: groups) {
			if (group.getGroupName().contains("\n") || group.getGroupName().contains("\r")) {
				groups.remove(group);
			}
		}

		return groups;
    }

    @Override
    public String getText(String key) {
        return bundle.getString(key);
    }

    public String getName() {
        return "InProductAccessManagement.jspa";
    }

    @Override
    protected String doExecute() throws Exception {
        wrm.requireResource("com.atlassian.translations.jira.inproduct:admin-css");
        wrm.requireResource("com.atlassian.translations.jira.inproduct:admin-javascript");
        boolean allowed = hasPermissions();
        if (allowed && request.getMethod().equals("POST")) {
            boolean ok = xsrfTokenGenerator.validateToken(request, request.getParameter("atl_token"));
            if (ok) {
                accessManager.setGroups(request.getParameterValues("groups"));
            } else {
                return PERMISSION_VIOLATION_RESULT;
            }
        }
        return allowed ? SUCCESS : PERMISSION_VIOLATION_RESULT;
    }
}
