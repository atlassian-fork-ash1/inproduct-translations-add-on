package com.atlassian.translations.inproduct;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

/**
 * User: kalamon
 * Date: 20.06.13
 * Time: 15:50
 */
public class JiraIptUtils {
    public static I18nHelper getI18nHelper() {
        return getComponentInstanceOfType(JiraAuthenticationContext.class).getI18nHelper();
    }

    public static ApplicationUser getCurrentUser() {
        return getComponentInstanceOfType(JiraAuthenticationContext.class).getUser();
    }

    public static <T> T getComponentInstanceOfType(final Class<T> clazz) {
        return ComponentAccessor.getComponentOfType(clazz);
    }
}
