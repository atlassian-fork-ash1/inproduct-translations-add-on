package com.atlassian.translations.inproduct.conditions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractJiraCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.translations.inproduct.components.PreviousLocaleManager;

/**
 * Do not use that condition except for tip popup. It uses PreviousLocaleManager which should be called only once per request.
 *
 * @since v5.0
 */
public class ShowTranslationTipCondition extends AbstractJiraCondition {

	private PreviousLocaleManager previousLocaleManager;

	public ShowTranslationTipCondition(PreviousLocaleManager previousLocaleManager) {
		this.previousLocaleManager = previousLocaleManager;
	}

	public boolean shouldDisplay(User user, JiraHelper jiraHelper) {
		return previousLocaleManager.hasLocaleChanged();
	}

	public boolean shouldDisplay(ApplicationUser applicationUser, JiraHelper jiraHelper) {
		return previousLocaleManager.hasLocaleChanged();
	}
}
