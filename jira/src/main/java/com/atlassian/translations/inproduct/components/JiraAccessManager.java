package com.atlassian.translations.inproduct.components;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.translations.inproduct.JiraIptUtils;
import com.atlassian.translations.inproduct.ao.AoAllowedGroupsManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * User: kalamon
 * Date: 17.07.12
 * Time: 11:01
 */
public class JiraAccessManager implements AccessManager {
    private final GroupManager groupManager;
    private final AoAllowedGroupsManager allowedGroupsManager;

    public JiraAccessManager(final GroupManager groupManager, final AoAllowedGroupsManager allowedGroupsManager) {
        this.groupManager = groupManager;
        this.allowedGroupsManager = allowedGroupsManager;
    }

    public static class GroupSpec {
        private boolean allowed;
        private String groupName;

        public GroupSpec(String groupName, boolean allowed) {
            this.groupName = groupName;
            this.allowed = allowed;
        }

        public boolean isAllowed() {
            return allowed;
        }

        public String getGroupName() {
            return groupName;
        }
    }

    public List<GroupSpec> getGroups() {
        Collection<String> allowedGroups = allowedGroupsManager.getAllowedGroups();
        boolean all = allowedGroups == null;
        List<GroupSpec> result = new ArrayList<GroupSpec>();
        Collection<Group> groups = groupManager.getAllGroups();
        for (Group group : groups) {
            result.add(new GroupSpec(group.getName(), all || allowedGroups.contains(group.getName())));
        }
        return result;
    }

    public void setGroups(String[] groups) {
        if (groups == null) {
            allowedGroupsManager.setAllowedGroups(null);
        } else {
            List<String> groupList = Arrays.asList(groups);
            allowedGroupsManager.setAllowedGroups(groupList);
        }
    }

    public boolean isUserAllowedToTranslate(String userName) {
        if (userName == null) {
            return false;
        }
        Collection<String> allowedGroups = allowedGroupsManager.getAllowedGroups();
        if (allowedGroups == null) {
            return true;
        }
        for (String allowedGroup : allowedGroups) {
            if (groupManager.isUserInGroup(userName, allowedGroup)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isCurrentUserAllowedToTranslate() {
        final ApplicationUser currentUser = JiraIptUtils.getCurrentUser();

        if (currentUser != null) {
            return isUserAllowedToTranslate(currentUser.getName());
        }

        return false;
    }
}
