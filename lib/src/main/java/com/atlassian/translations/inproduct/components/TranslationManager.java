package com.atlassian.translations.inproduct.components;

import java.util.Map;

/**
 * User: kalamon
 * Date: 10.07.12
 * Time: 15:06
 */
public interface TranslationManager {
    void saveTranslation(final String locale, final String key, final String translation);
    String getTranslation(final String locale, final String key);
    String deleteTranslation(final String locale, final String key) throws Exception;

	void deleteAllTranslations(String locale);

	boolean isAnyTranslation(String locale);

    Map<String, String> getAllTranslations(String locale);
}
