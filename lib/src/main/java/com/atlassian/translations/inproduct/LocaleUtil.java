package com.atlassian.translations.inproduct;

import java.util.Locale;

public class LocaleUtil {

	// must match supported locales in TAC
	public static Locale[] supportedLocales = {
            new Locale("ar", "SA"),
            new Locale("be", "BY"),
			new Locale("bg", "BG"),
			new Locale("ca", "ES"),
			new Locale("cs", "CZ"),
			new Locale("da", "DK"),
			new Locale("de", "DE"),
			new Locale("de", "CH"),
			new Locale("el", "GR"),
			new Locale("en", "US"),
			new Locale("en", "UK"),
			new Locale("es", "AR"),
			new Locale("es", "ES"),
			new Locale("es", "MX"),
			new Locale("et", "EE"),
			new Locale("fa", "IR"),
            new Locale("fi", "FI"),
			new Locale("fr", "FR"),
			new Locale("fr", "BE"),
			new Locale("fr", "CA"),
			new Locale("hi", "IN"),
			new Locale("hu", "HU"),
			new Locale("in", "ID"),
			new Locale("is", "IS"),
			new Locale("it", "IT"),
			new Locale("ja", "JP"),
			new Locale("ko", "KR"),
			new Locale("lt", "LT"),
			new Locale("lv", "LV"),
			new Locale("nl", "NL"),
			new Locale("nl", "BE"),
			new Locale("no", "NO"),
			new Locale("pl", "PL"),
			new Locale("pt", "PT"),
			new Locale("pt", "BR"),
			new Locale("ro", "RO"),
			new Locale("ru", "RU"),
			new Locale("sk", "SK"),
			new Locale("sr", "RS"),
			new Locale("sv", "SE"),
			new Locale("th", "TH"),
			new Locale("tr", "TR"),
			new Locale("uk", "UA"),
			new Locale("vi", "VN"),
			new Locale("zh", "CN"),
			new Locale("zh", "TW")
	};
}