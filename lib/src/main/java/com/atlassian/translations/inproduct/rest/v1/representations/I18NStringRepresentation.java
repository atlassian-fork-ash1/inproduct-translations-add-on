package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: kalamon
 * Date: 22.12.10
 * Time: 14:05
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
public class I18NStringRepresentation {

    @XmlElement private String key;
    @XmlElement private String value;

    public I18NStringRepresentation() {
    }

    public I18NStringRepresentation(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
