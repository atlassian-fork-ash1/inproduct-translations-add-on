package com.atlassian.translations.inproduct.components;

/**
 * User: kalamon
 * Date: 20.06.13
 * Time: 15:42
 */
public interface AccessManager {
    boolean isCurrentUserAllowedToTranslate();
}
