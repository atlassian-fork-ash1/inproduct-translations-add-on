package com.atlassian.translations.inproduct.ao.entity;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface AllowedGroupEntity extends Entity {
	String getName();
	void setName(String name);
}
