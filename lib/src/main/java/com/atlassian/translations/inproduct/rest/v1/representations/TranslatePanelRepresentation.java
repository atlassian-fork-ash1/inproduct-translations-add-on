package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: kalamon
 * Date: 02.07.12
 * Time: 16:27
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TranslatePanelRepresentation {
    @XmlElement private int x;
    @XmlElement private int y;
    @XmlElement private int w;
    @XmlElement private int h;

    public TranslatePanelRepresentation(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public TranslatePanelRepresentation() {
    }
}
