package com.atlassian.translations.inproduct.components;

import com.atlassian.translations.inproduct.components.TranslationsRequest.Product;
import com.atlassian.translations.inproduct.components.utils.AidLoginService;
import com.atlassian.translations.inproduct.components.utils.HttpRequestExecutor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * User: kalamon
 * Date: 12.04.13
 * Time: 12:17
 */
public class TacIntegrationManager {

    private static final HttpHost host = new HttpHost("jira.atlassian.com", 443, "https");
    private static final HttpHost tacHost = new HttpHost("translations.atlassian.com", 443, "https");
    private static final String base = "";
    private static final String projectId = "11460";
    private static final String label = "ipt_languagepack_import_request";
	private static final Logger LOG = Logger.getLogger(TacIntegrationManager.class);

    private static class CreateIssue {
        private Map<String, Object> fields;

        private CreateIssue(Map<String, Object> fields) {
            this.fields = fields;
        }
    }

    private static class CreatedIssue {
        private String self;
        private String id;
        private String key;
    }

    private final LanguagePackGenerator languagePackGenerator;
    private final TranslationManager translationManager;
    private final AidLoginService loginService;
    private final HttpRequestExecutor httpRequestExecutor;

    public TacIntegrationManager(final LanguagePackGenerator languagePackGenerator, final TranslationManager translationManager,
                                 final AidLoginService loginService, final HttpRequestExecutor httpRequestExecutor) {
        this.languagePackGenerator = languagePackGenerator;
        this.translationManager = translationManager;
        this.loginService = loginService;
        this.httpRequestExecutor = httpRequestExecutor;
    }

    public String requestTacUpload(String locale, String product, String productVersion, String login, String password) throws IOException {
        final String loginToken = loginService.login(login, password);
        final CreatedIssue issue = createIssue(locale, product, productVersion, loginToken);
        uploadLanguagePackTo(issue.key, languagePackGenerator.generate(locale), loginToken);
        return new Gson().toJson(issue);
    }

    public String publishTranslationsToTac(String locale, Map<String, String> productsAndVersions, String login, String password) throws IOException {
        final String loginToken = loginService.login(login, password);
        return uploadToTac(locale, productsAndVersions, loginToken);
    }

    private String uploadToTac(String locale, Map<String, String> productsAndVersions, String loginToken) throws IOException {
        final Map<String, String> translations = translationManager.getAllTranslations(locale);
        final List<Product> products = productsAndVersions.entrySet().stream()
                .map(entry -> new Product(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
        final TranslationsRequest requestBody = new TranslationsRequest(locale, products, translations);
        final String requestBodyJson = new Gson().toJson(requestBody);

        final StringEntity entity = new StringEntity(requestBodyJson, ContentType.APPLICATION_JSON);
        final HttpPost req = new HttpPost(base + "/api/uploadTranslations");
        req.addHeader("Accept", "application/json");
        req.addHeader("Content-Type", "application/json");
        req.setEntity(entity);

        return httpRequestExecutor.executeRequest(tacHost, req, loginToken);
    }

    private CreatedIssue createIssue(String locale, String product, String productVersion, String loginToken) throws IOException {

        String environment = "product: " + product + "\nversion: " + productVersion + "\nlocale: " + locale;
        Map<String, Object> fields = ImmutableMap.<String, Object>of(
            "project", ImmutableMap.<String, String>of("id", projectId),
            "issuetype", ImmutableMap.<String, String>of("id", "3"),
            "summary", "Language Pack Upload Request",
            "environment", environment,
            "labels", ImmutableList.of(label)
        );

        CreateIssue ci = new CreateIssue(fields);
        String gson = new Gson().toJson(ci);

        StringEntity entity = new StringEntity(gson, ContentType.APPLICATION_JSON);
        HttpPost req = new HttpPost(base + "/rest/api/2/issue");
        req.addHeader("Accept", "application/json");
        req.addHeader("Content-Type", "application/json");
        req.setEntity(entity);

        final String stringResult = httpRequestExecutor.executeRequest(host, req, loginToken);
        return new Gson().fromJson(stringResult, CreatedIssue.class);
    }

    private String uploadLanguagePackTo(String key, File file, String loginToken) throws IOException {
        MultipartEntity entity = new MultipartEntity();
        entity.addPart("file", new FileBody(file));
        HttpPost req = new HttpPost(base + "/rest/api/2/issue/" + key + "/attachments");

        req.addHeader("X-Atlassian-Token", "nocheck");
        req.setEntity(entity);

        return httpRequestExecutor.executeRequest(host, req, loginToken);
    }
}
