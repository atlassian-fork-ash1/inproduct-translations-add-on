package com.atlassian.translations.inproduct.rest.v1.resources;

import com.atlassian.translations.inproduct.LocaleUtil;
import com.atlassian.translations.inproduct.MessageTransformUtil;
import com.atlassian.translations.inproduct.TranslationValidator;
import com.atlassian.translations.inproduct.components.AccessManager;
import com.atlassian.translations.inproduct.components.MessageKeyGuesser;
import com.atlassian.translations.inproduct.components.TacIntegrationManager;
import com.atlassian.translations.inproduct.components.TranslationManager;
import com.atlassian.translations.inproduct.rest.v1.representations.ErrorStringRepresentation;
import com.atlassian.translations.inproduct.rest.v1.representations.I18NStringRepresentation;
import com.atlassian.translations.inproduct.rest.v1.representations.InitDataRepresentation;
import com.atlassian.translations.inproduct.rest.v1.representations.LocalesRepresentation;
import com.atlassian.translations.inproduct.rest.v1.representations.MessageKeyGuessRepresentation;
import com.atlassian.translations.inproduct.rest.v1.representations.MessageKeyGuessRepresentations;
import com.atlassian.translations.inproduct.rest.v1.representations.OriginalMessageWithNumberOfTranslationsRepresentation;
import com.atlassian.translations.inproduct.rest.v1.representations.OriginalMessageWithTranslationRepresentation;
import com.atlassian.translations.inproduct.rest.v1.representations.StringsRepresentation;
import org.apache.http.client.HttpResponseException;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public abstract class AbstractTranslationResource {
	private TranslationManager translationManager;
	private final MessageKeyGuesser guesser;
	private final AccessManager accessManager;
    private final TacIntegrationManager tacIntegrationManager;

    private static final ResourceBundle jsBundle = ResourceBundle.getBundle("i18n.jsi18n");

	private final List<I18NStringRepresentation> strings;

	private static final Logger LOG = Logger.getLogger(AbstractTranslationResource.class);

    protected abstract void clearWebCache();
    protected abstract boolean isCurrentUserAdmin();
    protected abstract Locale getCurrentLocale();
    protected abstract String getUnescapedText(String key);
    protected abstract String getUntransformedRawText(String key);
    protected abstract String getProduct();
    protected abstract String getProductVersion();
	protected abstract Map<String, String> getProductsAndVersions();

	protected AbstractTranslationResource(
            TranslationManager translationManager, MessageKeyGuesser guesser,
            AccessManager accessManager, TacIntegrationManager tacIntegrationManager) {
		this.translationManager = translationManager;
		this.guesser = guesser;
		this.accessManager = accessManager;
        this.tacIntegrationManager = tacIntegrationManager;
        strings = new ArrayList<I18NStringRepresentation>();
		Enumeration<String> keys = jsBundle.getKeys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			strings.add(new I18NStringRepresentation(key, jsBundle.getString(key)));
		}
	}

	public Response getInitData() {
		boolean isAnyTranslation = translationManager.isAnyTranslation(getCurrentLocale().toString());
		boolean allowed = accessManager.isCurrentUserAllowedToTranslate();
		return Response.ok(
                new InitDataRepresentation(
                    allowed, isAnyTranslation, isCurrentUserAdmin(), getProduct())
                ).build();
	}

	public Response getStrings() {
		return Response.ok(new StringsRepresentation(strings)).build();
	}

	public Response getLocales() {
		return Response.ok(new LocalesRepresentation(getCurrentLocale(), Arrays.asList(LocaleUtil.supportedLocales))).build();
	}

	public Response getOriginalMessage(final String messageKey) {

		String message = getUntransformedRawText(messageKey);

		String translation = MessageTransformUtil.stripDecoration(getUnescapedText(messageKey));

		return Response.ok(new OriginalMessageWithTranslationRepresentation(message, translation)).build();
	}

    public Response toggleTranslationMode() {
        if (!accessManager.isCurrentUserAllowedToTranslate()) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        try {
            clearWebCache();
            return Response.ok().build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
    }

	private static class KeysAndMessages {
		private String key;
		private String message;
		private int index;

		private KeysAndMessages(String key, String message, int index) {
			this.key = key;
			this.message = message;
			this.index = index;
		}

		private KeysAndMessages() {
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		@Override
		public String toString() {
			return "KeysAndMessages{" +
					"key='" + key + '\'' +
					", message='" + message + '\'' +
					", index=" + index +
					'}';
		}
	}

	public Response guessKeys(String keysAndMessagesJson) {
		ObjectMapper om = new ObjectMapper();
		try {
			List<KeysAndMessages> kms = om.readValue(keysAndMessagesJson, new TypeReference<List<KeysAndMessages>>() {
			});
			List<MessageKeyGuesser.KeyAndMessageQueryAndResult> query = new ArrayList<MessageKeyGuesser.KeyAndMessageQueryAndResult>();
			for (KeysAndMessages km : kms) {
				String keyFragment = km.getKey();
                String message = km.getMessage();
                boolean keyEmpty = keyFragment == null || keyFragment.startsWith(MessageKeyGuesser.NULL_KEY_FRAGMENT);
                if (!keyEmpty && keyFragment.endsWith("...")) {
					keyFragment = keyFragment.substring(0, keyFragment.lastIndexOf("..."));
				} else if (keyEmpty && message.endsWith("...")) {
                    message = message.substring(0, message.lastIndexOf("..."));
                }
				query.add(new MessageKeyGuesser.KeyAndMessageQueryAndResult(keyFragment, message, km.getIndex()));
//                LOG.warn(km);
			}
			guesser.find(getCurrentLocale(), query);
			ArrayList<MessageKeyGuessRepresentation> keyGuesses = new ArrayList<MessageKeyGuessRepresentation>();
			for (MessageKeyGuesser.KeyAndMessageQueryAndResult item : query) {
				if (item.getKeyMatches().size() == 1) {
					keyGuesses.add(new MessageKeyGuessRepresentation(item.getKeyFragment(), item.getKeyMatches().get(0), item.getIndex()));
				}
			}
			return Response.ok(new MessageKeyGuessRepresentations(keyGuesses)).build();
		} catch (IOException e) {
			LOG.warn(e.getMessage(), e);
			return Response.status(Response.Status.BAD_REQUEST).entity(e.toString()).build();
		}
	}

	public Response validateTranslation(String original, String translation) {

		String error = TranslationValidator.validate(original, translation);

		if (error.isEmpty()) {
			return Response.ok(true).build();
		} else {
			return Response.ok(new ErrorStringRepresentation(error)).build();
		}
	}

	public Response saveTranslation(final String messageKey, final String original,	final String translation, final String locale) {

		if (!accessManager.isCurrentUserAllowedToTranslate()) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}

		String error = TranslationValidator.validate(original, translation);

		if (error.isEmpty()) {
			// todo add error handling for save
			translationManager.saveTranslation(locale, messageKey, translation);
            clearWebCache();
        } else {
			// validation failed
			return Response.ok(new ErrorStringRepresentation(error)).build();
		}

		return Response.ok(true).build();
	}

	public Response deleteTranslation(final String messageKey, final String locale) {

		if (!accessManager.isCurrentUserAllowedToTranslate()) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}

		if (messageKey == null || messageKey.length() == 0) {
			return Response.ok(new ErrorStringRepresentation("missing message key")).build();
		} else if (locale == null || locale.length() == 0) {
			return Response.ok(new ErrorStringRepresentation("missing locale")).build();
		}

		try {
            String message = translationManager.deleteTranslation(locale, messageKey);
            clearWebCache();
            return Response.ok(new OriginalMessageWithNumberOfTranslationsRepresentation(
                    message,
					translationManager.isAnyTranslation(locale))).build();
		} catch (Exception e) {
			return Response.ok(new ErrorStringRepresentation(e.getMessage())).build();
		}
	}

	public Response deleteAllTranslations(final String locale) {

		if (!accessManager.isCurrentUserAllowedToTranslate()) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}

		if (locale == null || locale.length() == 0) {
			return Response.ok(new ErrorStringRepresentation("missing locale")).build();
		}

		translationManager.deleteAllTranslations(locale);
        clearWebCache();

        return Response.ok(true).build();
	}

    public Response uploadToTac(final String locale, final String login, final String password) {

        try {
            String response = tacIntegrationManager.requestTacUpload(locale, getProduct(), getProductVersion(), login, password);
            return Response.ok(response).build();
        } catch (HttpResponseException e) {
            return Response.status(Response.Status.fromStatusCode(e.getStatusCode())).entity(e.getMessage()).build();
        } catch (IOException e) {
			LOG.warn(e.getMessage(), e);
			return Response.ok(new ErrorStringRepresentation(e.getMessage())).build();
		} catch (Exception e) {
            LOG.warn(e.getMessage(), e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

	public Response contributeToTac(final String locale, final String login, final String password) {

		try {
			String response = tacIntegrationManager.publishTranslationsToTac(locale, getProductsAndVersions(), login, password);
			return Response.ok(response).build();
		} catch (HttpResponseException e) {
			return Response.status(Response.Status.fromStatusCode(e.getStatusCode())).entity(e.getMessage()).build();
		} catch (IOException e) {
			LOG.warn(e.getMessage(), e);
			return Response.ok(new ErrorStringRepresentation(e.getMessage())).build();
		} catch (Exception e) {
			LOG.warn(e.getMessage(), e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
	}
}
