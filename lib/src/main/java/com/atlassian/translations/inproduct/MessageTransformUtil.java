package com.atlassian.translations.inproduct;

import org.apache.log4j.Logger;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;

import java.util.regex.Pattern;

public class MessageTransformUtil {

    private static final Logger LOG = Logger.getLogger(MessageTransformUtil.class);

    // todo strip also local translation marker
	public static String stripDecoration(String translationDecorated) {

		String translation = translationDecorated;

		int start = translation.indexOf(AbstractMessageTransform.START_HIGHLIGHT_CHAR);
		int end = translation.indexOf(AbstractMessageTransform.MIDDLE_HIGHLIGHT_CHAR);

		if (start >= 0 && end >= 0 && start < end) {
			translation = translation.substring(start + 1, end);
		}

		int start2 = translation.indexOf(AbstractMessageTransform.LOCAL_TRANSLATION_CHAR);

		if (start2 >= 0) {
			translation = translation.substring(start2 + 1);
		}
		return translation;
	}

    public static String sanitize(String text) {
        return POLICY.sanitize(text);
    }

    public static String unquoteMoustaches(String text) {
        return text.replace("&#39;{", "'{").replace("&#39;}", "'}");
    }

    private static final PolicyFactory POLICY = new HtmlPolicyBuilder()
        .allowStandardUrlProtocols()
            // Allow title="..." on any element.
        .allowAttributes("title").globally()
            // Allow href="..." on <a> elements.
        .allowAttributes("href").onElements("a")
            // Defeat link spammers.
        .requireRelNofollowOnLinks()
            // Allow lang= with an alphabetic value on any element.
        .allowAttributes("lang").matching(Pattern.compile("[a-zA-Z]{2,20}"))
        .globally()
            // The align attribute on <p> elements can have any value below.
        .allowAttributes("align")
        .matching(true, "center", "left", "right", "justify", "char")
        .onElements("p")
            // These elements are allowed.
        .allowElements(
            "a", "p", "div", "i", "b", "em", "blockquote", "tt", "strong",
            "br", "ul", "ol", "li")
        .toFactory();
}
