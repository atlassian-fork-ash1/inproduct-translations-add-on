package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: kalamon
 * Date: 03.07.12
 * Time: 10:41
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageKeyGuessRepresentation {
    @XmlElement private String keyFragment;
    @XmlElement private String key;
    @XmlElement private int index;

    public MessageKeyGuessRepresentation(String keyFragment, String key, int index) {
        this.keyFragment = keyFragment;
        this.key = key;
        this.index = index;
    }

    public MessageKeyGuessRepresentation() {
    }
}
